﻿namespace Faez.TestAssessment.UnitTests.Parsers
{
    using System;
    using System.Collections.Generic;
    using Assessment.Core.Parsers;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public sealed class AccountParserTests
    {
        [TestCaseSource(nameof(GetTestCases))]
        public void ParsingBehaviour(string value, bool success)
        {
            var parser = new AccountParser();

            parser.TryParse(value, out _).Should().Be(success);
        }

        [Test]
        public void ThrowsException()
        {
            var nullInput = new Action(() => new AccountParser().TryParse(null, out _));
            var whitespaceInput = new Action(() => new AccountParser().TryParse(" ", out _));

            nullInput.ShouldThrow<ArgumentException>();
            whitespaceInput.ShouldThrow<ArgumentException>();
        }

        private static IEnumerable<TestCaseData> GetTestCases()
        {
            yield return new TestCaseData("12345678", true).SetName("Normal");
            yield return new TestCaseData("123456789999", true).SetName("WithCheckDigits");
            yield return new TestCaseData("12345", false).SetName("InvalidInput");
        }
    }
}
