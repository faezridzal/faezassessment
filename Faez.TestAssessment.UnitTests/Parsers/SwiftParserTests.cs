﻿namespace Faez.TestAssessment.UnitTests.Parsers
{
    using System;
    using System.Collections.Generic;
    using Assessment.Core.Parsers;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public sealed class SwiftParserTests
    {
        [TestCaseSource(nameof(GetTestCases))]
        public void ParsingBehaviour(string value, bool success)
        {
            var parser = new SwiftParser();

            parser.TryParse(value, out _).Should().Be(success);
        }

        [Test]
        public void ThrowsException()
        {
            var nullInput = new Action(() => new SwiftParser().TryParse(null, out _));
            var whitespaceInput = new Action(() => new SwiftParser().TryParse(" ", out _));

            nullInput.ShouldThrow<ArgumentException>();
            whitespaceInput.ShouldThrow<ArgumentException>();
        }

        private static IEnumerable<TestCaseData> GetTestCases()
        {
            yield return new TestCaseData("DEUTDEFF", true).SetName("Normal");
            yield return new TestCaseData("BSAMLKLXXXX", true).SetName("WithBranchCode");
            yield return new TestCaseData("MBBEMY", false).SetName("InvalidInput");
        }
    }
}
