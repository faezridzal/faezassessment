﻿namespace Faez.TestAssessment.UnitTests.Parsers
{
    using System;
    using System.Collections.Generic;
    using Assessment.Core.Parsers;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public sealed class RoutingParserTests
    {
        [TestCaseSource(nameof(GetTestCases))]
        public void ParsingBehaviour(string value, bool success)
        {
            var parser = new RoutingParser();

            parser.TryParse(value, out _).Should().Be(success);
        }

        [Test]
        public void ThrowsException()
        {
            var nullInput = new Action(() => new RoutingParser().TryParse(null, out _));
            var whitespaceInput = new Action(() => new RoutingParser().TryParse(" ", out _));

            nullInput.ShouldThrow<ArgumentException>();
            whitespaceInput.ShouldThrow<ArgumentException>();
        }

        private static IEnumerable<TestCaseData> GetTestCases()
        {
            yield return new TestCaseData("111000025", true).SetName("Normal");
            yield return new TestCaseData("111000028", false).SetName("WrongChecksum");
            yield return new TestCaseData("11100002", false).SetName("InvalidInput");
        }
    }
}
