﻿namespace Faez.Assessment.Console.Models
{
    internal sealed class PaymentInformation
    {
        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public string SwiftCode { get; set; }
    }
}
