﻿namespace Faez.Assessment.Console.Models
{
    using System.Collections.Generic;
    using Core.Models;

    internal sealed class EnrichedPaymentInformation
    {
        public RoutingInformation RoutingInformation { get; set; }

        public Account Account { get; set; }

        public Swift Swift { get; set; }

        public IEnumerable<string> Remarks { get; set; }
    }
}
