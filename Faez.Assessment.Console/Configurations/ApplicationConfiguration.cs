﻿namespace Faez.Assessment.Console.Configurations
{
    internal sealed class ApplicationConfiguration
    {
        public string InputFile { get; set; }

        public string OutputFile { get; set; }
    }
}
