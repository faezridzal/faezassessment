﻿namespace Faez.Assessment.Console.Configurations
{
    using System.IO;
    using Core.Models;
    using Core.Parsers;
    using Helpers;
    using Infrastructure;
    using Process;
    using Unity;
    using Unity.Injection;
    using Unity.Lifetime;

    internal static class UnityConfiguration
    {
        public static IUnityContainer Configure()
        {
            var container = new UnityContainer();
            var configuration = GetConfiguration();

            container.RegisterInstance(configuration);

            var inputArguments = new InjectionConstructor(configuration.InputFile);
            var outputArguments = new InjectionConstructor(configuration.OutputFile);

            container.RegisterType<IInputReader, InputFileReader>(new ContainerControlledLifetimeManager(), inputArguments);
            container.RegisterType<IOutputWriter, OutputFileWriter>(new ContainerControlledLifetimeManager(),outputArguments);
            container.RegisterType<IEnricher, AmericanPaymentEnricher>(new ContainerControlledLifetimeManager());
            container.RegisterType<IParser<Account>, AccountParser>(new ContainerControlledLifetimeManager());
            container.RegisterType<IParser<RoutingInformation>, RoutingParser>(new ContainerControlledLifetimeManager());
            container.RegisterType<IParser<Swift>, SwiftParser>(new ContainerControlledLifetimeManager());

            return container;
        }

        private static ApplicationConfiguration GetConfiguration()
        {
            var outputPath = Path.Combine(PathHelper.GetExecutingAssemblyPath(), "Files\\Input");

            Directory.CreateDirectory(outputPath);

            return new ApplicationConfiguration
            {
                InputFile = Path.Combine(PathHelper.GetExecutingAssemblyPath(), "Files\\Input", "payments-information.csv"),
                OutputFile = Path.Combine(Path.Combine(PathHelper.GetExecutingAssemblyPath(), "Files\\Output", "enriched-information.json"))
            };
        }
    }
}
