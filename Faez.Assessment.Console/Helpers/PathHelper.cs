﻿namespace Faez.Assessment.Console.Helpers
{
    using System;
    using System.IO;
    using System.Reflection;

    internal static class PathHelper
    {
        public static string GetExecutingAssemblyPath()
        {
            var codeBase = Assembly.GetExecutingAssembly().CodeBase;
            var uri = new UriBuilder(codeBase);
            var path = Uri.UnescapeDataString(uri.Path);

            return Path.GetDirectoryName(path);
        }
    }
}
