﻿namespace Faez.Assessment.Console.Process
{
    using Models;

    internal interface IEnricher
    {
        EnrichedPaymentInformation Enrich(PaymentInformation paymentInformation);
    }
}