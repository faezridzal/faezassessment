﻿namespace Faez.Assessment.Console.Process
{
    using System.Collections.Generic;
    using Core.Infrastructure.Extensions;
    using Core.Models;
    using Core.Parsers;
    using Models;

    internal sealed class AmericanPaymentEnricher : IEnricher
    {
        private readonly IParser<RoutingInformation> _routingParser;
        private readonly IParser<Account> _accountParser;
        private readonly IParser<Swift> _swiftParser;

        public AmericanPaymentEnricher(
            IParser<RoutingInformation> routingParser, 
            IParser<Account> accountParser, 
            IParser<Swift> swiftParser)
        {
            _routingParser = routingParser.EnsureArgumentNotNull(nameof(routingParser));
            _accountParser = accountParser.EnsureArgumentNotNull(nameof(accountParser));
            _swiftParser = swiftParser.EnsureArgumentNotNull(nameof(swiftParser));
        }

        public EnrichedPaymentInformation Enrich(PaymentInformation paymentInformation)
        {
            paymentInformation.EnsureArgumentNotNull(nameof(paymentInformation));

            var remarks = new List<string>();
            var result = new EnrichedPaymentInformation { Remarks = remarks };

            if (_routingParser.TryParse(paymentInformation.RoutingNumber, out var micr))
            {
                result.RoutingInformation = micr;
            }
            else
            {
                remarks.Add($"Cannot obtain MICR from value {paymentInformation.RoutingNumber}");
            }

            if (_accountParser.TryParse(paymentInformation.AccountNumber, out var usAccount))
            {
                result.Account = usAccount;
            }
            else
            {
                remarks.Add($"Cannot obtain account from value {paymentInformation.AccountNumber}");
            }

            if (_swiftParser.TryParse(paymentInformation.SwiftCode, out var swift))
            {
                result.Swift = swift;
            }
            else
            {
                remarks.Add($"Cannot obtain SWIFT from value {paymentInformation.SwiftCode}");
            }

            return result;
        }
    }
}
