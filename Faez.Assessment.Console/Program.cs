﻿namespace Faez.Assessment.Console
{
    using System;
    using System.Linq;
    using Configurations;
    using Infrastructure;
    using Process;
    using Unity;

    internal class Program
    {
        private static void Main()
        {
            Console.WriteLine("Reading customer inputs, performing enrichments and writing to file");

            using (var container = UnityConfiguration.Configure())
            {
                var fileReader = container.Resolve<IInputReader>();
                var fileWriter = container.Resolve<IOutputWriter>();
                var enricher = container.Resolve<IEnricher>();
                
                fileWriter.WriteAll(fileReader.ReadAll().Select(enricher.Enrich));
            }

            Console.WriteLine("Process complete. Press any key to exit.");
            Console.ReadLine();
        }
    }
}
