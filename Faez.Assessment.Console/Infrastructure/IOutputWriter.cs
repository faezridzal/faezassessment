﻿namespace Faez.Assessment.Console.Infrastructure
{
    using System.Collections.Generic;
    using Models;

    internal interface IOutputWriter
    {
        void WriteAll(IEnumerable<EnrichedPaymentInformation> enrichedPaymentInformation);
    }
}