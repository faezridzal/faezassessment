﻿namespace Faez.Assessment.Console.Infrastructure
{
    using System.Collections.Generic;
    using System.IO;
    using Core.Infrastructure.Extensions;
    using CsvHelper;
    using CsvHelper.Configuration;
    using Models;

    internal sealed class InputFileReader : IInputReader
    {
        private static readonly Configuration CsvConfiguration = new Configuration { HasHeaderRecord = true };

        private readonly string _file;

        public InputFileReader(string file)
        {
            _file = file.EnsureArgumentNotNullOrWhitespace(nameof(file));

            if (!File.Exists(_file))
            {
                throw new FileNotFoundException();
            }
        }

        public IEnumerable<PaymentInformation> ReadAll()
        {
            using (var stream = File.OpenText(_file))
            using (var reader = new CsvReader(stream, CsvConfiguration))
            {
                foreach (var paymentInformation in reader.GetRecords<PaymentInformation>())
                {
                    yield return paymentInformation;
                }
            }
        }
    }
}
