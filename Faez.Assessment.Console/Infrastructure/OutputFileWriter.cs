﻿namespace Faez.Assessment.Console.Infrastructure
{
    using System.Collections.Generic;
    using System.IO;
    using Core.Infrastructure.Extensions;
    using Models;
    using Newtonsoft.Json;

    internal sealed class OutputFileWriter : IOutputWriter
    {
        private readonly string _file;

        public OutputFileWriter(string file)
        {
            _file = file.EnsureArgumentNotNullOrWhitespace(nameof(file));
        }

        public void WriteAll(IEnumerable<EnrichedPaymentInformation> enrichedPaymentInformation)
        {
            var directoryName = Path.GetDirectoryName(_file);

            Directory.CreateDirectory(directoryName);

            var serializedObject = JsonConvert.SerializeObject(enrichedPaymentInformation, Formatting.Indented);

            File.WriteAllText(_file, serializedObject);
        }
    }
}
