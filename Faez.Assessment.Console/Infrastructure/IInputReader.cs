﻿namespace Faez.Assessment.Console.Infrastructure
{
    using System.Collections.Generic;
    using Models;

    internal interface IInputReader
    {
        IEnumerable<PaymentInformation> ReadAll();
    }
}