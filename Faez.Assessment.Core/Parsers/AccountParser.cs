﻿namespace Faez.Assessment.Core.Parsers
{
    using System.Text.RegularExpressions;
    using Infrastructure.Extensions;
    using Models;

    public sealed class AccountParser : IParser<Account>
    {
        private static readonly Regex Pattern = new Regex("^([0-9]{8})([0-9]{4})?$", RegexOptions.Compiled);

        public bool TryParse(string value, out Account account)
        {
            value.EnsureArgumentNotNullOrWhitespace(nameof(value));

            var result = Pattern.Match(value);

            if (result.Success)
            {
                account = new Account(result.Groups[1].Value);
                return true;
            }

            account = null;
            return false;
        }
    }
}
