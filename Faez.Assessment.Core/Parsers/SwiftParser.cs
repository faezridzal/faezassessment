﻿namespace Faez.Assessment.Core.Parsers
{
    using System.Text.RegularExpressions;
    using Infrastructure.Extensions;
    using Models;

    public sealed class SwiftParser : IParser<Swift>
    {
        private static readonly Regex Pattern = new Regex("^([a-zA-Z]{4})([a-zA-z]{2})([a-zA-Z0-9]{2})([a-zA-Z0-9]{3})?$", RegexOptions.Compiled);

        public bool TryParse(string value, out Swift swift)
        {
            value.EnsureArgumentNotNullOrWhitespace(nameof(value));

            var result = Pattern.Match(value);

            if (result.Success)
            {
                swift = new Swift(result.Groups[1].Value, result.Groups[2].Value, result.Groups[3].Value, result.Groups[4].Value);
                return true;
            }

            swift = null;
            return false;
        }
    }
}
