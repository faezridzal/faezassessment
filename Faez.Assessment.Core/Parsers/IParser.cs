﻿namespace Faez.Assessment.Core.Parsers
{
    public interface IParser<T> where T : class
    {
        bool TryParse(string value, out T account);
    }
}
