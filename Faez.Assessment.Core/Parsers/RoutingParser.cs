﻿namespace Faez.Assessment.Core.Parsers
{
    using System.Linq;
    using System.Text.RegularExpressions;
    using Infrastructure.Extensions;
    using Models;

    public sealed class RoutingParser : IParser<RoutingInformation>
    {
        private static readonly Regex Pattern = new Regex("^([0-9]{4})([0-9]{4})([0-9])$", RegexOptions.Compiled);

        public bool TryParse(string value, out RoutingInformation routingInformation)
        {
            value.EnsureArgumentNotNullOrWhitespace(nameof(value));

            var result = Pattern.Match(value);

            if (result.Success && IsChecksumValid(value))
            {
                routingInformation = new RoutingInformation(result.Groups[1].Value, result.Groups[2].Value, result.Groups[3].Value);
                return true;
            }
            
            routingInformation = null;
            return false;
        }

        private static bool IsChecksumValid(string value)
        {
            var digits = value.Select(c => (int)char.GetNumericValue(c)).ToArray();

            return
                (3 * (digits[0] + digits[3] + digits[6])
                 + 7 * (digits[1] + digits[4] + digits[7]) 
                 + digits[2] + digits[5] + digits[8])
                 % 10 
                 == 0;
        }
    }
}
