﻿namespace Faez.Assessment.Core.Models
{
    using Infrastructure.Extensions;

    public sealed class Swift
    {
        public Swift(string bankCode, string countryCode, string locationCode, string branchCode)
        {
            BankCode = bankCode.EnsureArgumentNotNullOrWhitespace(nameof(bankCode));
            CountryCode = countryCode.EnsureArgumentNotNullOrWhitespace(nameof(countryCode));
            LocationCode = locationCode.EnsureArgumentNotNullOrWhitespace(nameof(locationCode));
            BranchCode = string.IsNullOrWhiteSpace(branchCode) ? null : branchCode;
        }

        public string BankCode { get; }

        public string CountryCode { get; }

        public string LocationCode { get; }

        public string BranchCode { get; }
    }
}
