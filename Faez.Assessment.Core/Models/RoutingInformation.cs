﻿namespace Faez.Assessment.Core.Models
{
    using Infrastructure.Extensions;

    public sealed class RoutingInformation
    {
        public RoutingInformation(string symbol, string institution, string checkDigit)
        {
            Symbol = symbol.EnsureArgumentNotNullOrWhitespace(symbol);
            Institution = institution.EnsureArgumentNotNullOrWhitespace(nameof(institution));
            CheckDigit = checkDigit.EnsureArgumentNotNullOrWhitespace(nameof(checkDigit));
        }

        public string Symbol { get; }

        public string Institution { get; }

        public string CheckDigit { get; }
    }
}
