﻿namespace Faez.Assessment.Core.Models
{
    using Infrastructure.Extensions;

    public sealed class Account
    {
        public Account(string number)
        {
            Number = number.EnsureArgumentNotNullOrWhitespace(nameof(number));
        }

        public string Number { get; }
    }
}
