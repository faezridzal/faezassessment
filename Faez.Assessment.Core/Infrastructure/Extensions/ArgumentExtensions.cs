﻿namespace Faez.Assessment.Core.Infrastructure.Extensions
{
    using System;

    public static class ArgumentExtensions
    {
        public static T EnsureArgumentNotNull<T>(this T value, string argumentName)
        {
            if (value == null)
            {
                throw new ArgumentException(argumentName);
            }

            return value;
        }

        public static string EnsureArgumentNotNullOrWhitespace(this string value, string argumentName)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                throw new ArgumentException("Argument cannot be null or whitespace", argumentName);
            }

            return value;
        }
    }
}
